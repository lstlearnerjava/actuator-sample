package com.lst.shashank.gitlabsample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabSampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(GitlabSampleApplication.class, args);
	}

}
